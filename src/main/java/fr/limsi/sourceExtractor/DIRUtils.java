package fr.limsi.sourceExtractor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.channels.FileChannel;
import java.util.HashSet;

import org.apache.commons.io.IOUtils;

import com.google.common.base.Charsets;
import com.google.common.io.Files;


public class DIRUtils {

	/**
	 * Méthode qui permet de créer un dossier s'il n'existe pas puis de créer un fichier .tag dans ce dossier avec le nom name et l'extension ext.
	 * 
	 * @param dir Le chemin du répertoire à créer.
	 * @param name Le nom du fichier à créer.
	 * @param ext L'extension que l'on veut donner au fichier
	 * @return Le fichier créé.
	 * @throws IOException
	 */
	public synchronized static File createDirAndFilesWithExt(File dir, String name, String ext) throws IOException {
		
		File outFile = new File(dir, name + ext);

		if (!dir.exists()) {
			if (!dir.mkdirs()) {
//			outFile = new File(dir+"/"+name + ext);
//			
//			if (outFile.getParentFile().mkdirs()) {
//			    outFile.createNewFile();
//			}
//			else 
			    throw new IOException("Failed to create directory " + dir);
			}
		}
		else {
			if (outFile.exists())
				outFile.delete();
		}
		
		return outFile;
		
	}
	
	public synchronized static void deleteFilesInDirIfExist(File dir, String name, String ext) {
		if (new File(dir+"/"+name+ext).exists())
			new File(dir+"/"+name+ext).delete();
		
	}
	
	public synchronized static void deleteDirIfExists(File dir) {
		if (dir.exists())
			dir.delete();
		
	}
	
//	public synchronized static void createDir(File dir) {
//		dir.mkdirs();
//	}
	
//	public static boolean isDirEmpty(final Path directory) throws IOException {
//	    try(DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory)) {
//	        return !dirStream.iterator().hasNext();
//	    }
//	}
	/**
	 * Méthode qui permet de merger (assembler) en un fichier tous les fichiers d'un dossier.
	 * 
	 * Utilisée pour Wapiti (phase d'entrainement et de dev)
	 * 
	 * @param filesToMergeDir Le dossier où se trouvent les fichiers à assembler.
	 * @param outDir Le fichier en sortie.
	 * @throws IOException
	 */
	public synchronized static void mergeFile(File filesToMergeDir, File outDir) throws IOException {
		
		if (new File(outDir, filesToMergeDir.getName()+".txt").exists())
			new File(outDir, filesToMergeDir.getName()+".txt").delete();
		
			FileWriter fw = new FileWriter(new File(outDir, filesToMergeDir.getName()+".txt"));
		    BufferedWriter bw = new BufferedWriter(fw);
		    PrintWriter out = new PrintWriter(bw);

			for (File inFile : filesToMergeDir.listFiles()) {
				if (inFile.isFile()) {
					String text = Files.toString(inFile, Charsets.UTF_8);
					out.write("");
					out.append(text);
					out.append("\n");
				}
			}
			out.close();

	}
	
	/**
	 * Méthode qui permet de copier dans un dossier tous les fichiers qui ont le même nom que les fichiers d'un autre dossier.<br />
	 * <br />
	 * Utile pour utiliser BratEval (on copie tous les fichiers annotés originaux pour les comparer avec les fichiers annotés en BRAT automatiquement).<br />
	 * 
	 * @param dirFilenameToKeep Le dossier des fichiers annotés automatiquement.
	 * @param dirStackOfFiles Le dossier où se trouvent les fichiers originaux (ne marchent que pour les fichiers Labeled que l'on peut comparer).
	 * @param dirDestination Le dossier où on copiera les fichiers.
	 * @throws IOException
	 */
	public synchronized static void copyFilesWithSameNameToCompare(File dirFilenameToKeep, File dirStackOfFiles, File dirDestination) throws IOException {
		HashSet<String> nameOfTestFiles = new HashSet<>();
		
		for (File inFile : dirFilenameToKeep.listFiles()) {
			if (inFile.isFile()) {
				String filename = inFile.getName();
				
				int pos = filename.lastIndexOf(".");
				if (pos > 0) {
				    filename = filename.substring(0, pos);
				}
				nameOfTestFiles.add(filename);
				
			}
		}
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		for (File inFile : dirStackOfFiles.listFiles()) {
			
			if (inFile.isFile()) {
				String filename = inFile.getName();
				
				int pos = filename.lastIndexOf(".");
				if (pos > 0) {
				    filename = filename.substring(0, pos);
				}
				
				if (nameOfTestFiles.contains(filename)) {
						DIRUtils.deleteDirIfExists(dirDestination);
						dirDestination.mkdirs();
						if (inFile.getName().endsWith(".ann") || inFile.getName().endsWith(".json_temp")) {
							inputChannel = new FileInputStream(inFile).getChannel();
							outputChannel = new FileOutputStream(dirDestination+"/"+inFile.getName()).getChannel();
							outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
							inputChannel.close();
							outputChannel.close();
						}
				}
			}
		}
	}
	
	/**
	 * Méthode qui permet de copier dans un dossier tous les fichiers qui ont le même nom que les fichiers d'un autre dossier.<br />
	 * <br />
	 * Utile pour utiliser BratEval (on copie tous les fichiers annotés originaux pour les comparer avec les fichiers annotés en BRAT automatiquement).<br />
	 * 
	 * @param dirFilenameToKeep Le dossier des fichiers annotés automatiquement.
	 * @param dirStackOfFiles Le dossier où se trouvent les fichiers originaux (ne marchent que pour les fichiers Labeled que l'on peut comparer).
	 * @param dirDestination Le dossier où on copiera les fichiers.
	 * @throws IOException
	 */
	public synchronized static void copyAndStreamedFilesWithSameNameToCompare(File dirFilenameToKeep, File dirStackOfFiles, OutputStream outputStream) throws IOException {
		HashSet<String> nameOfTestFiles = new HashSet<>();
		
		for (File inFile : dirFilenameToKeep.listFiles()) {
			if (inFile.isFile()) {
				String filename = inFile.getName();
				
				int pos = filename.lastIndexOf(".");
				if (pos > 0) {
				    filename = filename.substring(0, pos);
				}
				nameOfTestFiles.add(filename);
				
			}
		}
		for (File inFile : dirStackOfFiles.listFiles()) {
			
			if (inFile.isFile()) {
				String filename = inFile.getName();
				
				int pos = filename.lastIndexOf(".");
				if (pos > 0) {
				    filename = filename.substring(0, pos);
				}
				
				if (nameOfTestFiles.contains(filename)) {
						if (inFile.getName().endsWith(".ann") || inFile.getName().endsWith(".json_temp")) {
							IOUtils.copy(new FileInputStream(inFile), outputStream);
						}
				}
			}
		}
		//delete dir
		//dirStackOfFiles.delete();
	}
	public synchronized static void copyAnnotationToStream(String pathToCopy, OutputStream outputStream) throws FileNotFoundException, IOException{
		File fileToCopy = new File(pathToCopy);
		if (fileToCopy.exists() && (fileToCopy.getName().endsWith(".ann") || fileToCopy.getName().endsWith(".json_temp"))) {
			IOUtils.copy(new FileInputStream(fileToCopy), outputStream);
		}
	}
	
	 public synchronized void deleteFiles( String d, String e ) {
		 
	     ExtensionFilter filter = new ExtensionFilter(e);
	     File dir = new File(d);
	 
	     String[] list = dir.list(filter);
	     File file;
	     if (list.length == 0) return;
	 
	     for (int i = 0; i < list.length; i++) {
	       file = new File(d + list[i]);
	       boolean isdeleted =   file.delete();
	       System.out.print(file);
	       System.out.println( "  deleted " + isdeleted);
	     }
	   }
	 
	   class ExtensionFilter implements FilenameFilter {
	 
	     private String extension;
	 
	     public ExtensionFilter( String extension ) {
	       this.extension = extension;             
	     }
	     public boolean accept(File dir, String name) {
	       return (name.endsWith(extension));
	     }
	   }

	
	
}
