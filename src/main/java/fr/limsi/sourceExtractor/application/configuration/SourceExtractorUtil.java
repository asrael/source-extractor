package fr.limsi.sourceExtractor.application.configuration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import fr.limsi.sourceExtractor.Memory;
import fr.limsi.sourceExtractor.Pair;
import fr.limsi.sourceExtractor.SourceAnnotation;

public class SourceExtractorUtil {

	public static String getDocText(String fileId, File file, Memory memory) throws IOException {
		String text = memory.docTexts.get(fileId);
		if (text == null) {
			text = Files.toString(file, Charsets.UTF_8);
			text = text.replaceAll("\\r", "");
			memory.docTexts.put(fileId, text);
		}
		return text;
	}

	/**
	 * inSec and outSEC can be the same
	 * 
	 * @param inSEC
	 * @param outSEC
	 * @param testMode
	 * @throws IOException
	 */
	public static void deleteSentenceWithoutPrimForSec(File inSEC, File outSEC, boolean testMode) throws IOException {
		if (inSEC.isDirectory()) {
			for (File file : inSEC.listFiles()) {
				deleteSentenceWithoutPrimForSec(file, new File(outSEC, file.getName()), testMode);
			}
		} else if (inSEC.isFile()) {
			Scanner scan = new Scanner(inSEC);
			StringBuilder result = new StringBuilder();

			StringBuilder sentenceBuffer = new StringBuilder();
			boolean foundPrim = false;

			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				if (line.length() > 1) {
					String[] fields = line.split("\\s+");
					String primClassName;
					if (testMode) {
						primClassName = fields[fields.length - 1];
					} else {
						primClassName = fields[SourceExtractorConstant.numberFieldsPrim];
					}

					assert primClassName.endsWith(SourceExtractorConstant.PRIM) || primClassName.equals(SourceExtractorConstant.OUT) : "You got the wrong column!";

					if (!primClassName.equals(SourceExtractorConstant.OUT)) {
						foundPrim = true;
					}
					sentenceBuffer.append(line + "\n");
				} else {
					if (foundPrim) {
						result.append(sentenceBuffer.toString());
						result.append("\n");
						foundPrim = false;
					}
					// else {
					// // add a newline so that we know that a sentence
					// // was skipped
					// output.write("\n");
					// }
					sentenceBuffer = new StringBuilder();
				}

			}
			scan.close();
			Files.write(result.toString(), outSEC, Charsets.UTF_8);
		} else {
			throw new RuntimeException();
		}
	}

	public static boolean isSuperSensePos(String cgPos) {
		boolean isCommonPos = cgPos.equals("N") || cgPos.equals("A") || cgPos.equals("D");
		return isCommonPos;
	}

	public static boolean isNPP(String posTag) {
		boolean isNPP = posTag.equals("NPP") || posTag.equals("ET");
		return isNPP;
	}

	/**
	 * @param mapOffsets
	 * @param source
	 * @param startSource
	 */
	public static void appendSource(TreeMap<Integer, Integer> sourceOffsets, HashMap<Integer, Pair<String, String>> wordsAndPosByOffset, StringBuilder source,
			int startSource) {

		Integer offsetEndSource = null;

		offsetEndSource = sourceOffsets.get(startSource);

		for (int offsetSubject = startSource; offsetSubject < offsetEndSource; offsetSubject++) {
			if (wordsAndPosByOffset.get(offsetSubject) != null)
				source.append(wordsAndPosByOffset.get(offsetSubject).getFirst() + " ");
		}
	}

	public static void appendSubject(TreeMap<Integer, Integer> mapOffset, TreeMap<Integer, Integer> sourceOffsets,
			HashMap<Integer, Pair<String, String>> wordsAndPosByOffset, HashMap<Integer, Pair<String, String>> coreference, StringBuilder source, int startSource,
			int startSubject, StringBuilder subject) {

		Integer endSubject = mapOffset.get(startSubject);

		for (Entry<Integer, Integer> entry : sourceOffsets.entrySet()) {
			Integer oneStartSource = entry.getKey();
			Integer oneEndSource = entry.getValue();

			// Wapiti is better to delimit the start and the end of a source
			// so if a subject has the same start offset as a source (or if the
			// subject is in the middle of the source),
			// we get the text of source instead of the subject text
			if (oneStartSource == startSubject || (oneStartSource < startSubject && endSubject < oneEndSource)) {
				startSubject = oneStartSource;
				endSubject = oneEndSource;
				break;
			}

		}

		// we build the subject from the index
		for (int j = startSubject; j < endSubject; j++) {
			Pair<String, String> wordAndPos = wordsAndPosByOffset.get(j);
			if (wordAndPos != null)
				subject.append(wordAndPos.getFirst() + " ");

		}

		coreference.put(startSource, new Pair<>(subject.toString(), source.toString()));
	}

	/**
	 * Get annotation from a string
	 * @param ann
	 * @return
	 */
	public static HashMap<String, SourceAnnotation> brat2Annotation(String ann) {
		HashMap<String, SourceAnnotation> result = new HashMap<>();
		String[] annLines = ann.split("\n");
		for (String annLine : annLines) {
			// Entity
			Matcher entityMatcher = SourceExtractorConstant.BRAT_ENTITY_DETAILED_PATTERN.matcher(annLine);
			if (entityMatcher.find()) {
				String id = entityMatcher.group(1);
				String type = entityMatcher.group(2);
				int leftOffset = Integer.parseInt(entityMatcher.group(3));
				int rightOffset = Integer.parseInt(entityMatcher.group(4));
				String text = entityMatcher.group(5);
				SourceAnnotation annotation = new SourceAnnotation(id, type, leftOffset, rightOffset, text);
				result.put(id, annotation);
			} else {
				// Comment
				Matcher commentMatcher = SourceExtractorConstant.BRAT_COMMENT_DETAILED_PATTERN.matcher(annLine);
				if (commentMatcher.find()) {
					String id = commentMatcher.group(1);
					String value = commentMatcher.group(2);
					SourceAnnotation annotation = result.get(id);
					assert annotation != null;
					if (value.equals("anonymous")) {
						annotation.setAnonymous(true);
					} else {
						annotation.setValue(value);
						String indexed = text2Index(value);
						if (indexed != null && indexed.length() > 0) {
							annotation.setIndexValue(indexed);
						}
					}
					result.put(id, annotation);
				}
			}
		}
		return result;
	}

	/**
	 * Get annotation from ann file.
	 * @param annFile
	 * @return
	 * @throws IOException
	 */
	public static HashMap<String, SourceAnnotation> brat2Annotation(File annFile) throws IOException {
		String ann = Files.toString(annFile, Charsets.UTF_8);
		return brat2Annotation(ann);
	}

	/**
	 * @param text
	 *            the normalized SOURCE, as labeled by Wapiti and resolved by coreference module
	 * @return the cleaned text to be indexed by later processes (search engines, etc...)
	 */
	private static String text2Index(String text) {
		String newText = text;
		Matcher m1 = SourceExtractorConstant.PN_COMMA_PATTERN.matcher(newText);

		if (m1.find()) {
			newText = m1.group(1);
		}

		Matcher m2 = SourceExtractorConstant.COMMA_NP_AND_MORE_PATTERN.matcher(newText);
		if (m2.find()) {
			newText = m2.group(1);
		}
		return newText;
	}

	public static int appendToJSON(String docText, JsonArray sentencesArray, int annIndex, ArrayList<SourceAnnotation> sentenceAnnotations, int currentSentenceIndex,
			int currentSentenceOffset, int nextSentenceOffset) {
		// System.out.println("substring " + currentSentenceOffset + " -> " +
		// (nextSentenceOffset - currentSentenceOffset));
		JsonObject sentenceObject = new JsonObject();
		JsonArray sourcesArray = new JsonArray();
		sentenceObject.addProperty("text", docText.substring(currentSentenceOffset, nextSentenceOffset));
		for (SourceAnnotation sentenceAnnotation : sentenceAnnotations) {
			sourcesArray.add(sentenceAnnotation.toJSON());
		}
		sentenceObject.add("sources", sourcesArray);
		sentencesArray.add(sentenceObject);
		annIndex++;
		return annIndex;
	}

	public static void putWordsAndPos(HashMap<Integer, Pair<String, String>> wordsAndPosByOffset, int offsetLeftItem, Integer itemRightOffset, ArrayList<String> sWords,
			ArrayList<String> sPOSs) {
		for (int offsetInSubject = offsetLeftItem; offsetInSubject < itemRightOffset; offsetInSubject++) {
			Pair<String, String> wordAndPOS = wordsAndPosByOffset.get(offsetInSubject);
			if (wordAndPOS != null) {
				sWords.add(wordAndPOS.getFirst());
				sPOSs.add(wordAndPOS.getSecond());
			}
		}
	}
}
