package fr.limsi.sourceExtractor.application.configuration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.maltparser.concurrent.ConcurrentMaltParserModel;
import org.maltparser.concurrent.ConcurrentMaltParserService;
import org.maltparser.core.exception.MaltChainedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import edu.stanford.nlp.ling.tokensregex.CoreMapExpressionExtractor;
import edu.stanford.nlp.ling.tokensregex.MatchedExpression;
import edu.stanford.nlp.ling.tokensregex.TokenSequencePattern;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import fr.limsi.sourceExtractor.FrenchLemmatizer;
import fr.limsi.sourceExtractor.IOUtils;
import fr.limsi.sourceExtractor.Memory;
import fr.limsi.sourceExtractor.Paths;
import fr.limsi.sourceExtractor.Resources;
import fr.limsi.sourceExtractor.Tools;
import fr.limsi.sourceExtractor.wapiti.WapitiLabeling;

@Component
@Configuration
public class SourceExtractorConfig {
	private Logger logger = LoggerFactory.getLogger(SourceExtractorConfig.class);

	private Paths paths;
	private File DATA_DIR, DIR_RESOURCES, DIR_LIB;

	@Value("${resource.lib}")
	private String resDirLib;
	@Value("${resource.resources}")
	private String resDirResources;
	@Value("${resource.data}")
	private String resDataLib;

	@Autowired
	private ServletContext servletContext;

	private boolean outputBrat = false;
	private boolean modelBio = true;
	private boolean searchSecondary = true;
	private boolean xml = false;

	private Resources resources;
	private Tools tools;
	private Memory memory;
	private File tempDir;

	private WapitiLabeling wapitiPrim, wapitiSec;

	@PostConstruct
	public void init() throws IOException, MaltChainedException, URISyntaxException {
		this.DATA_DIR = new File(resDataLib);
		this.DIR_LIB = new File(resDirLib);
		this.DIR_RESOURCES = new File(resDirResources);
		this.tempDir = (File) servletContext.getAttribute(ServletContext.TEMPDIR);
		FileUtils.forceDeleteOnExit(this.tempDir);
		String tempDir = this.tempDir.getAbsolutePath();
		logger.debug("temp dir: {}", tempDir);
		extractConfig(tempDir);
		loadTools();
//		loadWapitiNativeLib();
		loadWapitiModels("");
		//config();
	}

	@PreDestroy
	public void destroy() {
		logger.debug("destroy");
		try {
			FileUtils.forceDelete(this.tempDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public SourceExtractorConfig() {
		this.paths = new Paths();
		this.memory = new Memory(new ConcurrentHashMap<>(), new ConcurrentHashMap<>(), new ConcurrentHashMap<>(), new ConcurrentHashMap<>());
	}

	public SourceExtractorConfig(File dataDir, File dirLib, File dirResources) {
		this();
		this.DATA_DIR = new File(dataDir.getAbsolutePath());
		this.DIR_LIB = new File(dirLib.getAbsolutePath());
		this.DIR_RESOURCES = new File(dirResources.getAbsolutePath());
	}

	public void trainedConfig() {
		this.paths.DATA_LABELED_DIR = new File(DATA_DIR, "labeled");
		this.paths.DATA_UNLABELED_DIR = new File(DATA_DIR, "unlabeled");

		this.paths.DIR_INPUT_LABELED = new File(this.paths.DATA_LABELED_DIR, "input_labeled");
		this.paths.DIR_INPUT_LABELED_CONVERTED = new File(this.paths.DATA_LABELED_DIR, "input_labeled_converted");

		this.paths.DIR_INPUT_LABELED_MP = new File(this.paths.DATA_LABELED_DIR, "input_labeled_mp");
		this.paths.DIR_OUTPUT_LABELED = new File(this.paths.DATA_LABELED_DIR, "output_labeled");

		this.paths.DIR_INPUT_UNLABELED = new File(this.paths.DATA_UNLABELED_DIR, "input_unlabeled");
		this.paths.DIR_OUTPUT_UNLABELED = new File(this.paths.DATA_UNLABELED_DIR, "output_unlabeled");
		this.paths.DIR_INPUT_UNLABELED_MP = new File(this.paths.DATA_UNLABELED_DIR, "input_labeled_mp");

		this.paths.DIR_TRAIN_FILES_PRIM = new File(this.paths.DIR_OUTPUT_LABELED, "train_prim");
		this.paths.DIR_TEST_FILES_PRIM = new File(this.paths.DIR_OUTPUT_LABELED, "test_prim");
		this.paths.DIR_DEV_FILES_PRIM = new File(this.paths.DIR_OUTPUT_LABELED, "dev_prim");

		this.paths.DIR_TRAIN_FILES_SEC = new File(this.paths.DIR_OUTPUT_LABELED, "train_sec");
		this.paths.DIR_TEST_FILES_SEC = new File(this.paths.DIR_OUTPUT_LABELED, "test_sec");
		this.paths.DIR_DEV_FILES_SEC = new File(this.paths.DIR_OUTPUT_LABELED, "dev_sec");

		this.paths.DIR_TRAIN_FILES_UNLABELED_PRIM = new File(this.paths.DIR_OUTPUT_UNLABELED, "train_prim");
		this.paths.DIR_TEST_FILES_UNLABELED_PRIM = new File(this.paths.DIR_OUTPUT_UNLABELED, "test_prim");
		this.paths.DIR_DEV_FILES_UNLABELED_PRIM = new File(this.paths.DIR_OUTPUT_UNLABELED, "dev_prim");

		this.paths.DIR_TRAIN_FILES_UNLABELED_SEC = new File(this.paths.DIR_OUTPUT_UNLABELED, "train_sec");
		this.paths.DIR_TEST_FILES_UNLABELED_SEC = new File(this.paths.DIR_OUTPUT_UNLABELED, "test_sec");
		this.paths.DIR_DEV_FILES_UNLABELED_SEC = new File(this.paths.DIR_OUTPUT_UNLABELED, "dev_sec");

		this.paths.DIR_RESULT_FILES_LABELED = new File(this.paths.DATA_LABELED_DIR, "result_labeled");
		this.paths.DIR_MERGED_FILES_LABELED = new File(this.paths.DIR_RESULT_FILES_LABELED, "merged_files");

		this.paths.TRAIN_PRIM_MERGED_LABELED = new File(this.paths.DIR_MERGED_FILES_LABELED, "train_prim.txt");
		this.paths.DEV_PRIM_MERGED_LABELED = new File(this.paths.DIR_MERGED_FILES_LABELED, "dev_prim.txt");

		this.paths.TRAIN_SEC_MERGED_LABELED = new File(this.paths.DIR_MERGED_FILES_LABELED, "train_sec.txt");
		this.paths.DEV_SEC_MERGED_LABELED = new File(this.paths.DIR_MERGED_FILES_LABELED, "dev_sec.txt");

		this.paths.DIR_MODELS_LABELED = new File(this.paths.DIR_RESULT_FILES_LABELED, "models");

		this.paths.PATTERN_PRIM = new File(new File(DATA_DIR, "wapiti_patterns"), "pattern_prim.txt");
		this.paths.PATTERN_SEC = new File(new File(DATA_DIR, "wapiti_patterns"), "pattern_sec.txt");

		this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM = new File(this.paths.DIR_RESULT_FILES_LABELED, "bioWapiti_prim");
		this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_SEC = new File(this.paths.DIR_RESULT_FILES_LABELED, "bioWapiti_sec");

		this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED = new File(this.paths.DIR_RESULT_FILES_LABELED, "brat_autoWapiti");

		this.paths.DIR_BRAT_REFERENCE_LABELED = new File(this.paths.DIR_RESULT_FILES_LABELED, "brat_ref");

		this.paths.DIR_FINAL_RESULT_LABELED = new File(this.paths.DIR_RESULT_FILES_LABELED, "brat_ref_ajout_annotator");

		this.paths.DIR_RESULT_FILES_UNLABELED = new File(this.paths.DATA_UNLABELED_DIR, "result_unlabeled");

		this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "bioWapiti_prim");
		this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "bioWapiti_sec");

		this.paths.DIR_MERGED_FILES_UNLABELED = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "merged_files");

		this.paths.TRAIN_PRIM_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "train_prim.txt");
		this.paths.DEV_PRIM_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "dev_prim.txt");

		this.paths.TRAIN_SEC_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "train_sec.txt");
		this.paths.DEV_SEC_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "dev_sec.txt");

		this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "brat_autoWapiti");

		this.paths.DIR_FINAL_RESULT_UNLABELED = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "final");
		this.paths.MODEL_FINAL_DIR = new File(DIR_LIB, "wapiti_models");
	}

	public void config() {
		trainedConfig();
	}

	public void extractConfig(String tempDir) {
		this.paths.DATA_UNLABELED_DIR = new File(tempDir, "unlabeled");
		this.paths.DIR_RESULT_FILES_UNLABELED = new File(this.paths.DATA_UNLABELED_DIR, "result_unlabeled");
		this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "bioWapiti_prim");
		this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "bioWapiti_sec");
		this.paths.DIR_MERGED_FILES_UNLABELED = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "merged_files");
		this.paths.TRAIN_PRIM_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "train_prim.txt");
		this.paths.DEV_PRIM_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "dev_prim.txt");
		this.paths.TRAIN_SEC_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "train_sec.txt");
		this.paths.DEV_SEC_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "dev_sec.txt");
		this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "brat_autoWapiti");

		this.paths.DIR_OUTPUT_UNLABELED = new File(this.paths.DATA_UNLABELED_DIR, "output_unlabeled");
		this.paths.DIR_TEST_FILES_UNLABELED_PRIM = new File(this.paths.DIR_OUTPUT_UNLABELED, "test_prim");
		this.paths.DIR_TEST_FILES_UNLABELED_SEC = new File(this.paths.DIR_OUTPUT_UNLABELED, "test_sec");
		this.paths.DIR_INPUT_UNLABELED_MP = new File(this.paths.DATA_UNLABELED_DIR, "input_labeled_mp");
		this.paths.MODEL_FINAL_DIR = new File(DIR_LIB, "wapiti_models");
		this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED.mkdirs();
	}

	public void loadTools() throws IOException, MaltChainedException, URISyntaxException {
		String lemmatizerResourceDir = DIR_RESOURCES + "/fr/AhmetAkerLemmatizer_ressources/";
		String gazetteersDir = DIR_RESOURCES + "/fr/gazetteers/";
		File dirModelMaltParser = new File(new File(DIR_LIB, "maltparser"), "model");

		Set<String> citationVerbs = IOUtils.getFileContentAsSet(gazetteersDir + "/citation_verbs.lst");
		Set<String> professionWords = IOUtils.getFileContentAsSet(gazetteersDir + "/professions.lst");
		Set<String> countriesAndContinents = IOUtils.getFileContentAsSet(gazetteersDir + "/countries.lst");
		Set<String> stopWords = IOUtils.getFileContentAsSet(gazetteersDir + "/stopWords.lst");
		Set<String> persons = IOUtils.getFileContentAsSet(gazetteersDir + "/pers-org-triggers.lst");
		Set<String> capitals = IOUtils.getFileContentAsSet(gazetteersDir + "/capitals.lst");
		Set<String> personAbbr = IOUtils.getFileContentAsSet(gazetteersDir + "/person_abbr.lst");
		Set<String> pronouns = IOUtils.getFileContentAsSet(gazetteersDir + "/pronouns.lst");
		Set<String> temporalExpressions = IOUtils.getFileContentAsSet(gazetteersDir + "/temporal_expressions.lst");
		Set<String> nonDetArticles = IOUtils.getFileContentAsSet(gazetteersDir + "/undet-articles.lst");
		Map<String, String> coarseGrainedPOSMap = IOUtils.getFileContentAsMap(gazetteersDir + "/coarse_grained_category_mapping.lst", " ", false);

		this.resources = new Resources(citationVerbs, professionWords, countriesAndContinents, stopWords, persons, capitals, nonDetArticles, personAbbr, pronouns,
				temporalExpressions, coarseGrainedPOSMap);

		File stanfordExtractionRulesOthers = new File(DIR_RESOURCES + "/fr/citation-triggers.corenlp");
		File mediaListFile = new File(DIR_RESOURCES + "/fr/media-list.corenlp");
		File triggerSecondaryFile = new File(DIR_RESOURCES + "/fr/secondary-source-triggers.corenlp");

		StanfordCoreNLP coreNLPParser = getNewParser(Locale.FRENCH);
		FrenchLemmatizer lemmatizer = new FrenchLemmatizer(lemmatizerResourceDir);
		CoreMapExpressionExtractor<MatchedExpression> triggerExtractor = CoreMapExpressionExtractor.createExtractorFromFiles(TokenSequencePattern.getNewEnv(),
				stanfordExtractionRulesOthers.getAbsolutePath());

		CoreMapExpressionExtractor<MatchedExpression> mediaExtractor = null;
		if (searchSecondary) {
			mediaExtractor = CoreMapExpressionExtractor.createExtractorFromFiles(TokenSequencePattern.getNewEnv(), mediaListFile.getAbsolutePath());
		}

		CoreMapExpressionExtractor<MatchedExpression> triggerSecondaryExtractor = CoreMapExpressionExtractor.createExtractorFromFiles(TokenSequencePattern.getNewEnv(),
				triggerSecondaryFile.getAbsolutePath());

		logger.debug("Loading Malt parser... ");
		URL frMaltModelURL = new File(dirModelMaltParser, "fremalt-1.7.mco").toURI().toURL();
		ConcurrentMaltParserModel maltParserModel = ConcurrentMaltParserService.initializeParserModel(frMaltModelURL);
		this.tools = new Tools(triggerExtractor, mediaExtractor, triggerSecondaryExtractor, coreNLPParser, lemmatizer, maltParserModel);
		logger.debug("done");
	}

	private StanfordCoreNLP getNewParser(Locale locale) {
		Properties props = new Properties();
		props.put("tokenize.options", "tokenizeNLs=true");
		// props.put("ssplit.eolonly", "true");
		props.put("ssplit.newlineIsSentenceBreak", "always");
		props.put("parse.maxlen", "100");

		if (locale.equals(Locale.ENGLISH)) {
			// creates a StanfordCoreNLP object, with POS tagging,
			// lemmatization, NER, parsing, and coreference resolution
			props.put("annotators", "tokenize, ssplit, pos, lemma");
			props.put("ner.useSUTime", "false");

		} else if (locale.equals(Locale.FRENCH)) {
			props.put("pos.model", "edu/stanford/nlp/models/pos-tagger/french/french.tagger");

			props.put("parse.model", "edu/stanford/nlp/models/lexparser/frenchFactored.ser.gz");
			props.put("depparse.model", "edu/stanford/nlp/models/parser/nndep/UD_French.gz");

			props.put("annotators", "tokenize, ssplit, pos");
			props.put("tokenize.language", "fr");
		} else {
			throw new RuntimeException("Locale " + locale + " is not supported");
		}
		return new StanfordCoreNLP(props);
	}

//	public void loadWapitiNativeLib() {
//		logger.debug("Loading Wapiti library ");
//
//		// we load the wapiti library compiled for UNIX/POSIX
//		if (org.apache.commons.lang3.SystemUtils.IS_OS_LINUX) {
//			logger.debug("for Linux...");
//			System.load(DIR_LIB + "/wapiti_java/libwapiti.so");
//		} else if (SystemUtils.IS_OS_WINDOWS) {
//			// we load the wapiti library compiled for WINDOWS
//			logger.debug("for Windows...");
//			System.load(DIR_LIB + "/wapiti_java/libwapiti_swig.dll");
//			System.load(DIR_LIB + "/wapiti_java/libwapiti.dll");
//		} else if (SystemUtils.IS_OS_MAC) {
//			logger.debug("for MacOsx...");
//			System.load(DIR_LIB + "/wapiti_java/libwapiti.dylib");
//
//		} else {
//			logger.error("Cette version n'est pas prévue sur votre OS");
//			System.exit(1);
//		}
//		logger.debug(" done");
//	}

	/**
	 * Load model once at startup and reuse wapiti objects
	 * 
	 * @param modelSuffix
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public void loadWapitiModels(String modelSuffix) throws UnsupportedEncodingException, FileNotFoundException {
		File modelPrim = null;
		File modelSec = null;

		if (!modelBio) {
			modelPrim = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_PRIM + "_io" + modelSuffix);
			modelSec = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_SEC + "_io" + modelSuffix);
		} else {
			modelPrim = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_PRIM + "_bio" + modelSuffix);
			modelSec = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_SEC + "_bio" + modelSuffix);
		}
		wapitiPrim = WapitiLabeling.getWapitiInstance(modelPrim);
		wapitiSec = WapitiLabeling.getWapitiInstance(modelSec);
	}

	/**
	 * Méthode qui permet de transformer un fichier BRAT avec SOURCE vers un fichier BRAT avec SOURCE-PRIM et SOURCE-SEC
	 * 
	 * @throws IOException
	 */

	public boolean isOutputBrat() {
		return outputBrat;
	}

	public void setOutputBrat(boolean outputBrat) {
		this.outputBrat = outputBrat;
	}

	public boolean isModelBio() {
		return modelBio;
	}

	public void setModelBio(boolean modelBio) {
		this.modelBio = modelBio;
	}

	public boolean isSearchSecondary() {
		return searchSecondary;
	}

	public void setSearchSecondary(boolean searchSecondary) {
		this.searchSecondary = searchSecondary;
	}

	public Resources getResources() {
		return resources;
	}

	public Tools getTools() {
		return tools;
	}

	public Paths getPaths() {
		return paths;
	}
	
	public File getDirLib() {
		return DIR_LIB;
	}

	public Memory getMemory() {
		return memory;
	}

	public boolean isXml() {
		return xml;
	}

	public void setXml(boolean xml) {
		this.xml = xml;
	}

	public WapitiLabeling getWapitiPrim() {
		return wapitiPrim;
	}

	public WapitiLabeling getWapitiSec() {
		return wapitiSec;
	}

}
