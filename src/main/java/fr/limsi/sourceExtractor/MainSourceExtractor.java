package fr.limsi.sourceExtractor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.maltparser.core.exception.MaltChainedException;

import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConfig;
import fr.limsi.sourceExtractor.training.ExtractSource;
import fr.limsi.sourceExtractor.training.TrainingLabel;
import fr.limsi.sourceExtractor.training.TrainingUnLabel;

/**
 * Classe permettant de Tag, d'encoder en BIO, de convertir en Brat. Utilisant le Stanford CoreNLP.
 * Récriture de la classe principale utilisant les classes refactorées.
 * 
 * @author Bertrand Goupil
 *
 */
public class MainSourceExtractor {
	// Default configuration file
	private static final String DEFAULT_CONFIG_FILE = "src/main/resources/config.properties";

	// Configuration fields
	private static final String DATA_DIR_PROPERTY = "DATA_DIR";
	private static final String LIB_DIR_PROPERTY = "LIB_DIR";
	private static final String RESOURCES_DIR_PROPERTY = "RESOURCES_DIR";

	// Data directories (all will depend on directories specified
	// in configuration file)
	private static File DATA_DIR = null;
	private static File DIR_LIB = null;
	private static File DIR_RESOURCES = null;

	// private static File lastFinalFile;

	public MainSourceExtractor() throws IOException, MaltChainedException {

	}

	public static void main(String[] args) throws IOException, MaltChainedException, InterruptedException, URISyntaxException {
		// Debug mode
		if (args.length == 0) {
			// String argsStr = "-u -b -c
			// /home/xtannier/Recherche/SourceExtractor/config.properties";
			String argsStr = "-j 2 -o /tmp/test/ -d /home/xtannier/tmp/temp/seul/ -c /home/xtannier/Recherche/SourceExtractor/config.properties";
			args = argsStr.split(" ");
		}

		// Default parameter values
		String configFileName = DEFAULT_CONFIG_FILE;

		CLIParameters cli = new CLIParameters(args);

		// Property property = new Property();

		CommandLine cmd = cli.parse();
		// if (DATA_DIR == null) {
		// dataDirConfig();
		//
		// }
		if (cmd.hasOption(CLIParameters.OPTION_CONFIG)) {
			configFileName = cmd.getOptionValue(CLIParameters.OPTION_CONFIG);
		}

		Properties properties = new Properties();
		System.err.println("Loading file " + configFileName);
		properties.load(new FileInputStream(new File(configFileName)));

		DATA_DIR = new File(properties.getProperty(DATA_DIR_PROPERTY));
		DIR_LIB = new File(properties.getProperty(LIB_DIR_PROPERTY));
		DIR_RESOURCES = new File(properties.getProperty(RESOURCES_DIR_PROPERTY));

		SourceExtractorConfig config = new SourceExtractorConfig(DATA_DIR, DIR_LIB, DIR_RESOURCES);
		config.config();

		if (cmd.hasOption(CLIParameters.OPTION_BRAT)) {
			//outputBrat = true;
			config.setOutputBrat(true);
		}

		if (cmd.hasOption(CLIParameters.OPTION_IO)) {
			config.setModelBio(false);
			//modelBio = false;
		}

		String modelSuffix = "";
		if (cmd.hasOption(CLIParameters.OPTION_PRIM_ONLY)) {
			//searchSecondary = false;
			config.setSearchSecondary(false);
			modelSuffix = "_nomedia";
		}

		int jubNumber = 1;
		if (cmd.hasOption(CLIParameters.OPTION_JOB_NUMBER)) {
			jubNumber = Integer.parseInt(cmd.getOptionValue(CLIParameters.OPTION_JOB_NUMBER));
		}

		config.loadTools();

//		config.loadWapitiNativeLib();
		
		long startTime = System.currentTimeMillis();

		if (cmd.hasOption(CLIParameters.OPTION_DATA_SPLIT)) {
			TrainingLabel trainingLabel = new TrainingLabel(config);
			trainingLabel.splitTrainDevTest();
		}
		// if labeled
		else if (cmd.hasOption(CLIParameters.OPTION_DATA_TYPE_LABELED)) {
			TrainingLabel trainingLabel = new TrainingLabel(config);
			trainingLabel.train(modelSuffix, jubNumber);

		} else if (cmd.hasOption(CLIParameters.OPTION_DATA_TYPE_UNLABELED)) {
			//trainUnlabel
			TrainingUnLabel trainingUnLabel = new TrainingUnLabel(config);
			trainingUnLabel.train(modelSuffix, jubNumber);

		} else if (cmd.hasOption(CLIParameters.OPTION_FINAL_FILE) || cmd.hasOption(CLIParameters.OPTION_FINAL_DIR)) {
			//process source extraction
			if (!cmd.hasOption(CLIParameters.OPTION_OUTPUT_DIR)) {
				System.err.println("An output directory is required in annotation mode (-" + CLIParameters.OPTION_OUTPUT_DIR + ")");
				cli.printHelp(true);
			}
			File input;
			if (cmd.hasOption(CLIParameters.OPTION_FINAL_FILE)) {
				input = new File(cmd.getOptionValue(CLIParameters.OPTION_FINAL_FILE));
			} else {
				input = new File(cmd.getOptionValue(CLIParameters.OPTION_FINAL_DIR));
			}
			String outputDir = cmd.getOptionValue(CLIParameters.OPTION_OUTPUT_DIR);
			if (cmd.hasOption(CLIParameters.OPTION_NEWSML)) {
				config.setXml(true);
			}
			ExtractSource extractSource = new ExtractSource(config);
			extractSource.extractDirectory(modelSuffix, input, outputDir, jubNumber);
		} else {
			System.err.println("No parameter has been specified, did nothing");
		}
		long estimatedTime = System.currentTimeMillis() - startTime;
		System.err.println("Done in " + (estimatedTime / 1000.0 / 60.0) + " minutes");
	}
}