package fr.limsi.sourceExtractor;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.RecursiveTask;

public class PreprocessingTask extends RecursiveTask<Integer> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private File inFile;
	private boolean xmlInput;
	private	int countUnlabeled;
	private int nbFileUnlabeled;
	private Tools tools;
	private Resources resources;
	private Paths paths;
	private	Memory memory;
	

	public PreprocessingTask(File inFile, boolean xmlInput, int countUnlabeled, int nbFileUnlabeled, Tools tools,
			Resources resources, Paths paths, Memory memory) {
		super();
		this.inFile = inFile;
		this.xmlInput = xmlInput;
		this.countUnlabeled = countUnlabeled;
		this.nbFileUnlabeled = nbFileUnlabeled;
		this.tools = tools;
		this.resources = resources;
		this.paths = paths;
		this.memory = memory;
	}



	private static Integer preprocessUnlabeledFile(File inFile, boolean xmlInput, 
			int countUnlabeled, int nbFileUnlabeled, Tools tools, Resources resources, Paths paths,
			Memory memory)
			throws IOException {
		String filename = inFile.getName();
		String fileId = inFile.getName().substring(0, filename.lastIndexOf("."));

		System.err.println("Start labeling file : "+ countUnlabeled+"/"+(nbFileUnlabeled) + " " + fileId);

		File outFilePRIM = null;
		File outFileSEC = null;
		String text;
		File txtFile = new File(paths.DIR_INPUT_UNLABELED, fileId + ".txt");
		if (xmlInput) {
			text = SourceExtractor.extractTextFromXML(fileId, new File(paths.DIR_INPUT_UNLABELED, filename), txtFile, memory);
		}
		else {
			text = SourceExtractor.getDocText(fileId, txtFile, memory);
		}

		text = text.replaceAll("\\r", "");

		// we create the files in which we will write the results
		outFilePRIM = DIRUtils.createDirAndFilesWithExt(paths.DIR_TEST_FILES_UNLABELED_PRIM, fileId, ".tag");
		outFileSEC = DIRUtils.createDirAndFilesWithExt(paths.DIR_TEST_FILES_UNLABELED_SEC, fileId, ".tag");
		File outFileMP = DIRUtils.createDirAndFilesWithExt(paths.DIR_INPUT_UNLABELED_MP, fileId, ".conll");

		// parseText + maltParser + (b)io conversion
		SourceExtractor.tagAndConvert(fileId, inFile, text, outFileSEC, outFileMP, "", tools, resources, memory);

		// we delete the last column of (B)io files to find the SOURCE-PRIM
		SourceExtractor.deleteLastColumnForPrim(outFilePRIM, outFileSEC, true);
		outFileSEC.delete();
		return 1;
	}


	
	@Override
	protected Integer compute() {
		try {
			return preprocessUnlabeledFile(inFile, xmlInput, countUnlabeled, nbFileUnlabeled, tools, resources, paths, memory);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
