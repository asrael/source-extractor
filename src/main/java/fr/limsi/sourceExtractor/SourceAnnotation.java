package fr.limsi.sourceExtractor;

import com.google.gson.JsonObject;

public class SourceAnnotation implements Comparable<SourceAnnotation> {
	private String id;
	private int leftOffset;
	private int rightOffset;
	private String text;
	private String type;
	private String value;
	private String indexValue;
	private boolean anonymous;

	public SourceAnnotation(String id, String type, int leftOffset, int rightOffset, String text) {
		this.id = id;
		this.type = type;
		this.leftOffset = leftOffset;
		this.rightOffset = rightOffset;
		this.text = text;
	}
	
	public String getId() {
		return this.id;
	}
	
	public int getLeftOffset() {
		return leftOffset;
	}
	public void setLeftOffset(int leftOffset) {
		this.leftOffset = leftOffset;
	}
	public int getRightOffset() {
		return rightOffset;
	}
	public void setRightOffset(int rightOffset) {
		this.rightOffset = rightOffset;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getIndexValue() {
		return indexValue;
	}
	public void setIndexValue(String indexValue) {
		this.indexValue = indexValue;
	}
	public boolean isAnonymous() {
		return anonymous;
	}
	public void setAnonymous(boolean anynomous) {
		this.anonymous = anynomous;
	}

	@Override
	public int compareTo(SourceAnnotation o) {
		int c = Integer.compare(this.leftOffset, o.leftOffset);
		if (c != 0) {
			return c;
		}
		else {
			c = Integer.compare(this.rightOffset, o.rightOffset);
			if (c != 0) {
				return c;
			}
			else {
				return this.type.compareTo(o.type);
			}
		}
	}
	
	@Override
	public String toString() {
		return this.toAnn();
	}

	public String toAnn() {
		String main = "T" + this.id + "\t" + this.type + " " + this.leftOffset + " " + this.rightOffset + "\t" + this.text;
		if (this.value != null && this.value.length() > 0) {
			main += "\n" + "#" + this.id + "\tAnnotatorNotes T" + this.id + "\t" + this.value;
		}
		return main;
	}

	public JsonObject toJSON() {
		JsonObject main = new JsonObject();
		main.addProperty("start", leftOffset);
		main.addProperty("end", rightOffset);
		main.addProperty("type", type);
		main.addProperty("text", text.trim());

		if (value != null && value.trim().length() > 0) {
			main.addProperty("value", value.trim());
		}
		if (indexValue != null && indexValue.trim().length() > 0) {
			main.addProperty("indexed_value", indexValue.trim());
		}
		if (anonymous) {
			main.addProperty("anonymous", "yes");
		}
		return main;
	}
}
