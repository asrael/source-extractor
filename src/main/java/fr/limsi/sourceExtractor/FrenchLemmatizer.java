package fr.limsi.sourceExtractor;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.hfst.NoTokenizationException;
import net.sf.hfst.Transducer;
import net.sf.hfst.TransducerAlphabet;
import net.sf.hfst.TransducerHeader;
import net.sf.hfst.UnweightedTransducer;
import net.sf.hfst.WeightedTransducer;

public class FrenchLemmatizer {
	
    private static final Pattern NUMBER_PATTERN1 = Pattern.compile("\\d+[., ]\\d+");
    private static final Pattern NUMBER_PATTERN2 = Pattern.compile("\\d+(.)*");
    private static final Pattern PUNCT_PATTERN = Pattern.compile("\\p{Punct}+$", Pattern.CASE_INSENSITIVE);


	
    private Transducer transducer = null;
    private Map<String, String> nounDic;
    private Map<String, String> adjDic;
    private Map<String, String> advDic;
    private Map<String, String> verbDic;
    private Map<String, String> detDic;
    private Map<String, String> pronDic;
    private Map<String, String> posMap;

    public FrenchLemmatizer(String resourcesFolder) throws IOException {
		String language = "fr";
	    nounDic = IOUtils.loadDictionary(resourcesFolder + "/dictionaries/" + language + "/nounDic.txt");
	    adjDic = IOUtils.loadDictionary(resourcesFolder + "/dictionaries/" + language + "/adjDic.txt");
	    advDic = IOUtils.loadDictionary(resourcesFolder + "/dictionaries/" + language + "/advDic.txt");
	    verbDic = IOUtils.loadDictionary(resourcesFolder + "/dictionaries/" + language + "/verbDic.txt");
	    detDic = IOUtils.loadDictionary(resourcesFolder + "/dictionaries/" + language + "/detDic.txt");
	    pronDic = IOUtils.loadDictionary(resourcesFolder + "/dictionaries/" + language + "/pronounDic.txt");
	    
	    posMap = IOUtils.getFileContentAsMap(resourcesFolder + "/universal-pos-tags/" + language + "POSMapping.txt", "######", false);
	   
        FileInputStream transducerfile = null;
        transducerfile = new FileInputStream(resourcesFolder + "/lemmaModels/" + language + ".hfst.ol");
        TransducerHeader h = new TransducerHeader(transducerfile);
        DataInputStream charstream = new DataInputStream(transducerfile);
        TransducerAlphabet a = new TransducerAlphabet(charstream, h.getSymbolCount());
        if (h.isWeighted()) {
            transducer = new WeightedTransducer(transducerfile, h, a);
        } else {
            transducer = new UnweightedTransducer(transducerfile, h, a);
        }

    }
    
	private String getLemma2(String token, String generalType) throws NoTokenizationException {
        Collection<String> analyses = transducer.analyze(token);
        for (String analysis : analyses) {
        	String grammar = "NONE";
        	String grammarCheck = "NONE";
        	if ("NOUN".equals(generalType)) {
        		grammar = "\\+commonNoun.*";
        		grammarCheck = "+commonNoun";
        	} else if ("VERB".equals(generalType)) {
        		grammar = "\\+verb+.*";
        		grammarCheck = "+verb+";
        	} else if ("ADJ".equals(generalType)) {
        		grammar = "\\+adjective.*";
        		grammarCheck = "+adjective";
        	} else if ("ADV".equals(generalType)) {
        		grammar = "\\+adverb.*";
        		grammarCheck = "+adverb";
        	} else if ("PRON".equals(generalType) || "CONJ".equals(generalType)) {
        		grammar = "\\+functionWord.*";
        		grammarCheck = "+functionWord";

        	}
        	//System.out.println(analysis);
        	if (analysis.contains(grammarCheck)) {
        		String lemma = analysis.replaceAll(grammar, "");
        		if ((lemma.contains("+") && !lemma.contains("-")) && (token.contains("-") && !token.contains("+"))) {
        			lemma = lemma.replaceAll("\\+", "-");
        		}
        		if (lemma.contains("+") && !token.contains("+")) {
        			lemma = lemma.replaceAll("\\+", "");
        		}
        		return lemma.toLowerCase();
        	}
        }
        return null;		
	}

	
    private static boolean isNumber(String aWord) {
        Matcher m = NUMBER_PATTERN1.matcher(aWord);
        Matcher m2 = NUMBER_PATTERN2.matcher(aWord);
        return (m.find() || m2.find());
    }

    private static boolean isPunctuation(String aWord) {
        Matcher m = PUNCT_PATTERN.matcher(aWord);
        boolean match = m.matches();
        if (!match) {
            if (aWord.equals("\"") || aWord.equals("[") || aWord.equals("]")
                    || aWord.equals("{") || aWord.equals("}") || aWord.equals("(") || aWord.equals(")")
                    || aWord.equals("~") || aWord.equals("#") || aWord.equals("'") || aWord.equals("`") 
                    || aWord.equals("*") || aWord.equals("’") || aWord.equals("“") || aWord.equals("„") 
                    || aWord.equals("≥") || aWord.equals(">") || aWord.equalsIgnoreCase("<")) {
                return true;
            } 
        }
        return match;
    }
	
	public String getLemma(String token, String pos) {
        String generalType = posMap.get(pos);
        
//        System.out.println(generalType + " " + token);
//        if (pos.toLowerCase().equals("punc")) {
//        	System.out.println();
//        }
        assert generalType != null : "Unknown POS type " + pos + " (token " + token + ")";
    	String lemma = null;

        if (isNumber(token)) {
        	lemma = token;
        } else if (isPunctuation(token)) {
        	lemma = token.toLowerCase();
//        } else if (detDic.get(token.toLowerCase()) != null) {
//        	lemma = token.toLowerCase();
        } else {
        	if ("NOUN".equals(generalType)) {
        		lemma = nounDic.get(token.toLowerCase());
        	} else if ("VERB".equals(generalType)) {
        		lemma = verbDic.get(token.toLowerCase());
        	} else if ("ADJ".equals(generalType)) {
        		lemma = adjDic.get(token.toLowerCase());
        	} else if ("ADV".equals(generalType)) {
        		lemma = advDic.get(token.toLowerCase());
        	} else if ("PRON".equals(generalType)) {
        		lemma = pronDic.get(token.toLowerCase());
        	}

        	if (lemma == null) {
        		lemma = detDic.get(token.toLowerCase());
        	}
        	
        	if (lemma == null) {
	        	try {
	        		lemma = this.getLemma2(token, generalType);
	        	} catch (Exception e) {
	        		try {
	        			lemma = this.getLemma2(token.toLowerCase(), generalType);
	        		} catch (Exception e2) {
	        		}
	        	}
        	}
        	if (lemma == null || "".equals(lemma)) {
        		lemma = token;
        	} else {
        		if ("NOUN".equals(generalType)) {
        			String firstCharToken = token.substring(0, 1);
        			String firstCharLemma = lemma.toLowerCase().substring(0, 1);
        			if (!firstCharToken.equals(firstCharLemma) && firstCharToken.equalsIgnoreCase(firstCharLemma)) {
        				lemma = firstCharToken + lemma.substring(1).toLowerCase();
        			}
        		}
        	}
        }

        if (lemma.contains(" ")) {
                String[] tokens = lemma.split(" ");
                lemma = tokens[tokens.length - 1];
        }
      return lemma;

	}
	
	
	
//	public static void main(String[] args) throws IOException, NoTokenizationException {
//		String word = "Frédéric";
//		String pos = "NC";
//		FrenchLemmatizer lemmatizer = new FrenchLemmatizer("/home/xtannier/tools/AhmetAkerLemmatizer/Lemmatizer");
//		System.out.println(lemmatizer.getLemma(word, pos));
//	}

}
