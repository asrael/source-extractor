package fr.limsi.sourceExtractor.wapiti;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.RecursiveTask;

public class WapitiTask extends RecursiveTask<Integer> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private File input;
	private File outputDir;
	private WapitiLabeling wapitiLabeler;
	private File dirLib;
	
	public WapitiTask(File input, File outputDir, WapitiLabeling wapitiLabeler, File dirLib) {
		super();
		this.input = input;
		this.outputDir = outputDir;
		this.wapitiLabeler = wapitiLabeler;
		this.dirLib = dirLib;
	}

	private Integer wapitiLabelFile(File input, File outputDir, WapitiLabeling wapitiLabeler)
			throws UnsupportedEncodingException, FileNotFoundException {
		String filename = input.getName();

		int pos = filename.lastIndexOf(".");

		if (pos > 0) {
			filename = filename.substring(0, pos);
		}
		
		wapitiLabeler.label(input, outputDir, filename, this.dirLib);
		return 1;
	}



	
	@Override
	protected Integer compute() {
		try {
			return wapitiLabelFile(input, outputDir, wapitiLabeler);
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

}
