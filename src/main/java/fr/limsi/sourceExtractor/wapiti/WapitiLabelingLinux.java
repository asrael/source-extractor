package fr.limsi.sourceExtractor.wapiti;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WapitiLabelingLinux extends WapitiLabeling {
	private Logger logger = LoggerFactory.getLogger(WapitiLabelingLinux.class);

	private File modelFile;
	
	public WapitiLabelingLinux(File modelFile) {
		this.modelFile = modelFile;
	}

	public void label(File input, File outputDir, String filename, File dirLib)
			throws UnsupportedEncodingException, FileNotFoundException {
		if (!outputDir.exists()) {
			outputDir.mkdirs();
		}
		
		try {
			String command = String.format("%s/wapiti"
					+ " label -m "
					+ "%s "
					+ "-i %s "
					+ "-o %s -p", dirLib.getAbsoluteFile(), this.modelFile, input.getAbsolutePath(), 
					outputDir.getAbsolutePath() + "/" + filename + ".wapiti");
			logger.debug(command);
			
			String line;
			Process p = Runtime.getRuntime().exec(command);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
