package fr.limsi.sourceExtractor.training;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.google.common.io.Files;

import fr.limsi.sourceExtractor.DIRUtils;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConstant;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConfig;
import fr.limsi.sourceExtractor.wapiti.WapitiLabeling;

public class ExtractSource extends TrainingUnLabel {

	public ExtractSource(SourceExtractorConfig extractorConfig) {
		super(extractorConfig);
	}

	public void extractDirectory(String modelSuffix, File input, String outputDir, int jubNumber) throws IOException, InterruptedException {
		boolean xml = extractorConfig.isXml();
		this.paths.DIR_OUTPUT_FINAL = new File(outputDir);
		if (!this.paths.DIR_OUTPUT_FINAL.exists()) {
			this.paths.DIR_OUTPUT_FINAL.mkdirs();
		}

		PRODUCTION_MODE = true;

		File modelPrim = null;
		File modelSec = null;

		if (!modelBio) {
			modelPrim = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_PRIM + "_io" + modelSuffix);
			modelSec = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_SEC + "_io" + modelSuffix);
		} else {
			modelPrim = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_PRIM + "_bio" + modelSuffix);
			modelSec = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_SEC + "_bio" + modelSuffix);
		}

		if (input.isFile()) {
			this.paths.DIR_INPUT_FINAL = Files.createTempDir();
			this.paths.DIR_INPUT_FINAL.deleteOnExit();
			Files.copy(input, new File(this.paths.DIR_INPUT_FINAL, input.getName()));
			input = this.paths.DIR_INPUT_FINAL;
		}

		if (input.isDirectory()) {
			this.paths.DIR_INPUT_FINAL = input;

			File tempDir = Files.createTempDir();
			tempDir.deleteOnExit();

			this.paths.DIR_INPUT_UNLABELED = this.paths.DIR_INPUT_FINAL;

			//			this.paths.DATA_UNLABELED_DIR = new File(tempDir, "unlabeled");
			//			this.paths.DIR_RESULT_FILES_UNLABELED = new File(this.paths.DATA_UNLABELED_DIR, "result_unlabeled");
			//			this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "bioWapiti_prim");
			//			this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "bioWapiti_sec");
			//			this.paths.DIR_MERGED_FILES_UNLABELED = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "merged_files");
			//			this.paths.TRAIN_PRIM_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "train_prim.txt");
			//			this.paths.DEV_PRIM_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "dev_prim.txt");
			//			this.paths.TRAIN_SEC_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "train_sec.txt");
			//			this.paths.DEV_SEC_MERGED_UNLABELED = new File(this.paths.DIR_MERGED_FILES_UNLABELED, "dev_sec.txt");
			//			this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED = new File(this.paths.DIR_RESULT_FILES_UNLABELED, "brat_autoWapiti");
			//
			//			this.paths.DIR_OUTPUT_UNLABELED = new File(this.paths.DATA_UNLABELED_DIR, "output_unlabeled");
			//			this.paths.DIR_TEST_FILES_UNLABELED_PRIM = new File(this.paths.DIR_OUTPUT_UNLABELED, "test_prim");
			//			this.paths.DIR_TEST_FILES_UNLABELED_SEC = new File(this.paths.DIR_OUTPUT_UNLABELED, "test_sec");
			//			this.paths.DIR_INPUT_UNLABELED_MP = new File(this.paths.DATA_UNLABELED_DIR, "input_labeled_mp");

			this.paths.DIR_FINAL_RESULT_UNLABELED = this.paths.DIR_OUTPUT_FINAL;

			//			this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED.mkdirs();
			extractorConfig.extractConfig(tempDir.getAbsolutePath());

			// we start with tagging each files by launching maltparser on
			// these same files.
			// We eventually end with the (B)IO conversion.
			this.parseDirForUnlabeled(this.paths.DIR_INPUT_FINAL, xml, jubNumber);

			System.err.println("Labeling primary sources (Wapiti PRIM)");

			WapitiLabeling wapitiPrim = WapitiLabeling.getWapitiInstance(modelPrim);
			// wapiti label -m modelPrim input=tests_files_one_by_one
			// output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM -p
			wapitiPrim.wapitiTest(this.paths.DIR_TEST_FILES_UNLABELED_PRIM, this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM, jubNumber,
					modelPrim.getAbsolutePath(), extractorConfig.getDirLib().getAbsolutePath());

			if (searchSecondary) {
				System.err.println();
				System.err.println("Labeling secondary sources (Wapiti SEC)");
				deleteSentenceWithoutPrimForSec(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM, this.paths.DIR_TEST_FILES_UNLABELED_SEC, true);

				WapitiLabeling wapitiSec = WapitiLabeling.getWapitiInstance(modelSec);
				// wapiti label -m modelSec input=tests_files_one_by_one
				// output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_Sec
				// -p
				wapitiSec.wapitiTest(this.paths.DIR_TEST_FILES_UNLABELED_SEC, this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC, jubNumber,
						modelPrim.getAbsolutePath(), extractorConfig.getDirLib().getAbsolutePath());
			}

			System.err.println();
			System.err.println("Conversion Wapiti output -> Brat");

			// wapiti has predicted the labels, we now transform the (B)IO
			// files into Brat format.
			parseDirForUnlabeled(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM, false, jubNumber);
			if (searchSecondary) {
				parseDirForUnlabeled(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC, false, jubNumber);
			}

			System.err.println();
			System.err.println("Adding coreference and indexing information");

			// we end up adding some annotations to the previous files (the
			// ones that have been converted to brat)
			DIRUtils.copyFilesWithSameNameToCompare(this.paths.DIR_TEST_FILES_UNLABELED_PRIM, this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED,
					this.paths.DIR_FINAL_RESULT_UNLABELED);
			parseDirForUnlabeled(this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED, xml, jubNumber);
			FileUtils.deleteDirectory(tempDir);

			System.err.println("Output written in " + this.paths.DIR_OUTPUT_FINAL.getAbsolutePath());
		} else {
			System.err.println("File " + input + " is neither a file or a directory");
		}
	}

}
