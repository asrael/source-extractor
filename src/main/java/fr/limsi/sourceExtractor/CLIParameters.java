package fr.limsi.sourceExtractor;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CLIParameters {
	private static final Logger log = Logger.getLogger(CLIParameters.class.getName());
	private String[] args = null;
	private Options options = new Options();

	public static final String OPTION_DATA_SPLIT = "s";
	public static final String OPTION_DATA_TYPE_LABELED = "l";
	public static final String OPTION_CONFIG = "c";
	public static final String OPTION_DATA_TYPE_UNLABELED = "u";
	public static final String OPTION_FINAL_FILE = "f";
	public static final String OPTION_FINAL_DIR = "d";
	public static final String OPTION_OUTPUT_DIR = "o";
	public static final String OPTION_BRAT = "b";
	public static final String OPTION_NEWSML = "newsml";
	public static final String OPTION_IO = "io";
//	public static final String ARGUMENT_ENCODING = "encoding or ann";
	public static final String ARGUMENT_FINAL = "file";
	public static final String OPTION_HELP = "h";
	public static final String OPTION_PRIM_ONLY ="p";
	public static final String OPTION_JOB_NUMBER = "j";

	public CLIParameters(String[] args) {

		this.args = args;
		
		OptionGroup optionGroup = new OptionGroup();
		
		Option split = new Option(OPTION_DATA_SPLIT, "split", false, "split data into training/dev/test sets.");
		Option labeled = new Option(OPTION_DATA_TYPE_LABELED, "labeled", false, "if test files are labeled.");
//		labeled.setArgName(ARGUMENT_ENCODING);
		
		Option unlabeled = new Option(OPTION_DATA_TYPE_UNLABELED, "unlabeled", false, "if test files are unlabeled.");
//		unlabeled.setArgName(ARGUMENT_ENCODING);
		
		Option finalFile = new Option(OPTION_FINAL_FILE, "file", true, "annotate a file with a previously trained model");
		Option finalDir = new Option(OPTION_FINAL_DIR, "dir", true, "annotate a dir with a previously trained model");
//		finalFile.setArgName(ARGUMENT_FINAL+" [-bio]");
		Option outputDir = new Option(OPTION_OUTPUT_DIR, "output", true, "output directory (only with options " + OPTION_FINAL_FILE + " or " + OPTION_FINAL_DIR + ")");		
		
		Option help = new Option(OPTION_HELP, "help", false, "Show this help.");
		
		Option config = new Option(OPTION_CONFIG, "config", true, "set the directories configuration");
		
		Option io = new Option(OPTION_IO, "io", false, "Load or build IO model (instead of default BIO)"); 
		io.setRequired(false);
		
		Option brat = new Option(OPTION_BRAT, "brat", false, "output in Brat format (.ann)");
		brat.setRequired(false);
		
		Option primary = new Option(OPTION_PRIM_ONLY, "primary", false, "deactivate media lookup and secondary source extraction (faster initialization and process)");
		
		Option newsml = new Option(OPTION_NEWSML, false, "input files are in NewsML format (or in any XML format where the content is between <p></p>. Only in production mode.");
		
		Option jobNumber = new Option(OPTION_JOB_NUMBER, true, "job number (default is 1 -- no multi-threading)");
		
		optionGroup.addOption(split);
		optionGroup.addOption(labeled);
		optionGroup.addOption(finalFile);
		optionGroup.addOption(finalDir);
		optionGroup.addOption(unlabeled);
//		optionGroup.addOption(help);
//		optionGroup.addOption(config);
		
		optionGroup.setRequired(true);
		
		options.addOptionGroup(optionGroup);
		options.addOption(brat);	
		options.addOption(io);
		options.addOption(primary);
		options.addOption(outputDir);
		options.addOption(newsml);
		
		options.addOption(help);
		options.addOption(config);
		options.addOption(jobNumber);
	}

	public CommandLine parse() {
		CommandLineParser parser = new DefaultParser();

		CommandLine cmd = null;
		try {
			cmd = parser.parse(options, args);
			
		} catch (ParseException e) {
			log.log(Level.SEVERE, "Failed to parse command line properties", e);
			printHelp();
			System.exit(1);
		}
		
		return cmd;
	}

	public void printHelp() {
		printHelp(false);
	}
	
	public void printHelp(boolean quit) {
		// automatically generate the help statement
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("programName", options);
		if (quit) {
			System.exit(1);
		}
	}

}
