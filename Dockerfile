FROM frolvlad/alpine-java:jdk8-slim

MAINTAINER Duc Cao (tien-duc.cao@inria.fr)
RUN apk add maven
RUN apk add --no-cache gcc musl-dev
RUN apk add make

RUN mkdir /default

WORKDIR /default
COPY pom.xml pom.xml
COPY lib lib
RUN mvn dependency:go-offline -B
RUN mkdir data

COPY src src
COPY run.sh run.sh
COPY resources resources
COPY wapiti-1.5.0 wapiti-1.5.0

RUN mvn clean install
RUN cd /default/wapiti-1.5.0 && make wapiti && cp wapiti /default/lib
ENTRYPOINT ["/bin/sh", "run.sh"]
