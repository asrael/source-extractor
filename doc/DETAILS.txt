Auteur : Gabriel BELLARD, Stagiaire, LIMSI MAI/JUILLET 2016



INTRODUCTION 
------------------------------

Ce document est un guide complet pour la bonne utilisation et compréhension de l'application.

L'application SourceExtractor permet de prendre un fichier texte en entrée qui sera converti en une sortie JSON ou BRAT 
incluant toutes les sources (primaires et secondaires) de ce texte avec une gestion des coréférences en guise d'annotation.
Il y a deux versions du programme, une pour linux et une pour windows. 
La version de Linux contient toutes les options alors que la version de Windows ne contient que la dernière fonction (voir ci dessous).

INSTALLATION
------------------------------

	Télécharger l'archive
	Décompresser l'archive
	Se déplacer dans SourceExtractorLinux/ ou SourceExtractorWindows/
	Se déplacer dans app/
	Lancer l'application avec les commandes java -jar SourceExtractorLINUX.jar ou java -jar SourcesExtractorWindows.jar
	Configurer les dossiers
	Enjoy

COMMENT UTILISER LE PROGRAMME
------------------------------

Le programme est simple à utiliser en ligne de commande :

PREMIERE UTILISATION : 
	
	java -jar SourcesExtractor(OS).jar -c => Demande de dossier DATA puis demande de dossier TOOLS
	Dans mon archive DATA = Corpus/ et TOOLS = Outils/

DEJA PARAMETRE

	java -jar SourcesExtractor(OS).jar -l | -u io | bio | ann [-t brat] [-m]
	java -jar SourcesExtractor(OS).jar -f <file> [-t brat] [-m]

	Les librairies se trouvent dans un dossier à côté du .jar.

Les différentes options sont les suivantes : 

		-c 									: Permet de changer les dossiers de DATA pour placer les données et TOOLS pour les librairies et modèles.
										  	  Soit on lance pour la première fois l'application et on est obligé de rentrer un dossier, soit on le modifie avec cette option.

		-l <encoding ou ann> [-t brat] [-m] : Permet de lancer la phase d'apprentissage et la phase de test sur des fichiers déjà annotés.
											  Nous utilisons cette option pour vérifier que notre système fonctionne correctement.
											  Il prend en entrée un nombre n de fichiers annotés, les divise en 3 catégories (train, dev, test) 
											  à hauteur de 75%, 15%, 10% respectivement en les transformant dans l'encoding utilisé en argument
											  de l'option, lance la phase d'entrainement puis la phase de test, transforme les nouveaux fichiers 
											  en JSON ou BRAT.
											  S'ensuit une annotation automatique des sources par rapport aux sujets présents dans le texte.
											  ATTENTION LE MODELE EST MODIFIE A CHAQUE LANCER (IL SERA IDENTIQUE SI LES LES FICHIERS ET COLONNES (B)IO SONT IDENTIQUES)

											  -l io  => si on veut un encoding en io,
											  -l bio => si on veut un encoding en bio,
											  -l ann => si on veut seulement annoter les sujets et coréférences des sources obtenues (on doit préalablement
											  avoir fait une option au-dessus pour avoir les fichiers de sorties adéquats)

											  -t brat => change la sortie JSON par défaut en sortie BRAT.

											 			 Un script fourni en annexe lance BratEval et compare les résultats obtenus pour les sources trouvées
											 			 par rapport aux fichiers de référence puis écrit les scores obtenus en rappel et précision dans un 
											 			 fichier de sortie selon l'encoding utilisé.

		-u <encoding ou ann> [-t brat] [-m] : Permet de lancer la phase d'apprentissage sur des fichiers annotés et lancer la phase de test sur des fichiers non annotés.
											  Nous utilisons cette option pour vérifier que notre système fonctionne et reste cohérent avec des fichiers non annotés.
									          Le programme prend en entrée un nombre n1 de fichiers non annotés et un nombre n de fichiers annotés.
									          Il divise n en 2 catégories (train, dev) à hauteur de 90%, 10% respectivement en les transformant dans l'encoding utilisé en argument
									          de l'option et prend tous les fichiers non annotés pour la phase de test en les transformant, eux-aussi, selon l'encoding utilisé en argument.
									          La phase d'entrainement est lancée puis la phase de test et finalement transforme les nouveaux fichiers non annotés en JSON ou en BRAT.
									          S'ensuit une annotation automatique des sources par rapport aux sujets présents dans le texte.
											  ATTENTION LE MODELE EST MODIFIE A CHAQUE LANCER (IL SERA IDENTIQUE SI LES LES FICHIERS ET COLONNES (B)IO SONT IDENTIQUES)

											  -u io   => si on veut un encoding en io,
											  -u bio  => si on veut un encoding en bio,
											  -u ann  => si on veut seulement annoter les sujets et coréférences des sources obtenues (on doit préalablement
											             avoir fait une option au-dessus pour avoir les fichiers de sorties adéquats)

									  	      

		-f <file> [-bio] [-t brat] [-m]  :   Permet de lancer uniquement une phase de test sur un fichier .txt mis en argument.
											 Le fichier sera transformé en format (B)IO selon l'encoding de l'argument de l'option et ne contiendra que des O sur les dernières colonnes (aucune source annotée).
											 Nous chargeons un modèle sauvegardé correspondant à l'encoding et lançons la phase de test sur ce fichier. 
											 S'ensuit une annotation automatique des sources par rapport aux sujets présents dans le texte.

											 -f filename.txt => charge le modèle io et annote le fichier filename.txt,
											 -bio => charge le modèle bio et annote le fichier filename.txt,
		

		-t brat 					:        Change la sortie JSON par défaut en sortie BRAT.	

		-m 							: 	  	Permet de désactiver la recherche de médias dans les textes. Temps de chargement en moins et surtout permet de faire fonctionner la version windows ! 
											(du moins sur ma machine peut-être que j'ai un problème de buffer car le programme s'arrête sans aucune erreur... Il ne va pas jusqu'au bout)


		ATTENTION LA VERSION WINDOWS NE PEUT QUE FAIRE LA FONCTION FINALE -f !! PAS DE GENERATION DE MODELE SOUS WINDOWS !! DLL EN 64 BITS !!


SPECIFICITE
------------------------------								
 - COMMENT AJOUTER UNE COLONNE AU CRF :
 	
 	* Tout d'abord il faut changer le nombre de colonnes. 
 	Deux variables à modifier : 
		- private static final int numberFieldsPrim = 10; 
		- private static final int numberFieldsSec = 11;

	* Ensuite il faut ajouter dans parseText la colonne correspondant à ce que l'on veut faire.
		- Modifier seulement le if (!onlyOffset) { sentenceResult.append(A MODIFIER) } 

	* Dans la fonction tagFromMaltParserAndParseText, il faut modifier l'accès aux colonnes dans le String[] parsed = parsedTextLine.split("\\s+");

	* Modifier dans FeatureSet :
		- Le constructeur,
		- Le toString(),
		- La construction myText = new FeatureSet() dans tagFromMaltParserAndParseText

 - COMMENT EST GEREE L'ANNOTATION POUR LES SOURCES PRIMAIRES ET POUR LES SOURCES SECONDAIRES :

 	Lorsque nous transformons les fichiers en format (B)IO, nous gardons 2 colonnes à la fin pour annoter les SOURCE-SEC sur la dernière colonne et les SOURCE-PRIM sur l'avant-dernière colonne.
 	Pour l'entrainement des SOURCE-PRIM, nous supprimons la dernière colonne qui contient les SOURCE-SEC et nous lançons l'apprentissage.
 	Pour l'entrainement des SOURCE-SEC nous ajoutons au pattern du CRF la colonne des SOURCE-PRIM car une SOURCE-SEC n'est jamais présente sans SOURCE-PRIM.

- QUELS PATTERNS UTILISONS-NOUS :
	
	Nous utilisons deux patterns différents selon si souhaite entrainer les sources primaires ou les sources secondaires.

	Pour les sources primaires : 
		- Colonne UNIGRAM : word, PoS, lemma, isTrigger, isProf, QM, isMedia, mix_col
		- Colonne TRIGRAM : word, PoS, lemma, isTrigger, mix_col, isSuj, isSujTrigger

	Pour les sources secondaires : 
		- Colonne UNIGRAM : PoS, isTrigger, isProf, isMedia, SOURCE-PRIM
		- Colonne TRIGRAM : PoS, isTrigger, isMedia, mix_col, isSuj, isSujTrigger, SOURCE-PRIM

	Plusieurs tests ont été faits (et il va falloir en faire encore), les résultats ont montré qu'ajouter un BIGRAM donnait de moins bons résultats en général.

- QUE SONT LES MERGED FILES : 
	
	Les "merged files" sont utilisés pour la phase d'entrainement (les fichiers de tests assemblés "ne servent à rien"). Nous regroupons tous les fichiers de train et de dev
	ensemble pour que Wapiti entraine un modèle sur un long corpus.

- ALGORITHME DES COREFERENCES : 
	
	Faire pour les 3 TreeMap SubjectOffsets, profAndTriggerSecondaryOffsets et pronounsOffsets :

	Enregistrer tous les mots de la source
 (1)Si tous les mots de la source sont un nom de profession et que ce nom a déjà annoté en tant que source
		Alors annoter à cette source le même sujet qu'à ce nom déjà annoté
	Sinon
		Si le premier mot d'une source n'est pas article indéterminé
		|	Alors int bestSubjectIndex = -1
		|	Parcourir tous les offsets des sujets potentiels de la map en cours
		|	Si l'offset du sujet potentiel < l'offset de la source en cours (un annote un sujet à une souce seulement si le sujet arrive avant la source)
		|	|	Alors enregistrer tous les mots du sujet
		|	|		  Boolean contain = true
		|	|	      int countNumberOfSharedWords = 0
		|	|	      Parcourir tous les mots de la source en omettant les PERSON_ABBR et les stopwords
		|	|	      Si le sujet potentiel ne contient pas le mot en cours
		|	|		  	Alors contain = false
		|	|		  Sinon 
		|	|		  	countNumberOfSharedWords++
		|	|		  Parcourir tous les mots du sujet potentiel
		|	|		  Si le tokenSubject en cours n'est pas un stopword
		|	|		  |	Alors Parcourir tous les mots de la source
		|	|		  |		  Si le tokenSource en cours n'est pas un stopword
		|	|		  |		  	Alors Si tokenSubject = tokenSource et tokenSubject n'est pas un PERSON_ABBR
		|	|		  |		  	|		Si tokenSource est un NPP et que le sujet potentiel contient lui aussi un NPP
		|	|		  |		  	|		|	Alors Si contain
		|	|		  |		  	|		|		bestSubjectIndex = le sujet potentiel
		|	|		  |		  	|		Sinon Si tokenSource est un mot de profession 
		|	|		  |		  	|		|	Alors Si contain et countNumberOfSharedWords > 1
		|	|		  |		  	|		|		Alors Si l'offset du sujet potentiel != l'offset de la source
		|	|		  |		  	|		|		|	  	bestSubjectIndex = le sujet potentiel
		|	|		  |		  	|		|		|	  Sinon Si l'offset du sujet potentiel = l'offset de la source et que la source contient un NPP
		|	|		  |		  	|		|		|	  	Alors bestSubjectIndex = le sujet potentiel = la source elle-même car NPP
		|	|		  |		  	|		|		|	  Sinon 
		|	|		  |		  	|		|		|	  	bestSubjectIndex = le dernier sujet rencontré avant cette source s'il existe
		|	|		  |		  	|		|		Sinon Si contain et countNumberOfSharedWords = 1
		|	|		  |		  	|		|			Alors Si un sujet précédent cette source existe
		|	|		  |		  	|		|				Alors Si le sujet est dans la liste des sujets
		|	|		  |		  	|		|				|	Alors si le sujet précédent n'est pas la source elle-même
		|	|		  |		  	|		|				|		Alors bestSubjectIndex = le sujet précédent
		|	|		  |		  	|		|				|	Sinon Si un dernier sujet de cette ma rencontré avant cette source existe
		|	|		  |		  	|		|				|		Alors bestSubjectIndex = le dernier sujet rencontré
		|	|		  |		  	|		|				|			  On annote ce sujet en tant que nom de profession, il sert à (1)
		|	|		  |		  	|		|				Sinon Si le sujet est dans la liste des professions trouvées 
		|	|		  |		  	|		|					Alors si le sujet précédent n'est pas la source elle-même
		|	|		  |		  	|		|						Alors bestSubjectIndex = le sujet précédent
		|	|		  |		  	|		|					Sinon Si un dernier sujet de cette ma rencontré avant cette source existe
		|	|		  |		  	|		|						Alors bestSubjectIndex = le dernier sujet rencontré
		|	|		  |		  	|		Sinon Si tokenSource est un pronom
		|	|		  |		  	|		|	Alors Si un dernier sujet existe et que c'est un nom de profession
		|	|		  |		  	|		|		Alors bestSubjectIndex = le dernier sujet rencontré
		|	|		  |		  	|		|	Sinon 
		|	|		  |		  	|		|		On cherche le dernier sujet avant la source
		|	|		  |		  	|		Sinon Si tokenSource est un nom de personne (homme, femme, enfant...)
		|	|		  |		  	|				Alors si le sujet précédent n'est pas la source elle-même
		|	|		  |		  	|								Alors bestSubjectIndex = le sujet précédent
		|	|		  |		  	|							Sinon Si un dernier sujet de cette ma rencontré avant cette source existe
		|	|		  |		  	|								Alors bestSubjectIndex = le dernier sujet rencontré
		|	|		  |		  	Sinon Si tokenSource contient un pronom
		|	|		  |		  		Alors Si un dernier sujet existe 
		|	|		  |		  			Alors bestSubjectIndex = le dernier sujet rencontré
		|	|		  |		  		Sinon On cherche dans la liste des professions
		|	|		  Si on est dans la liste des sujets
		|	|		  	on enregistre le dernier sujet rencontré
		|	Si un meilleur sujet pour cette source existe
		|		Alors si ce sujet a déjà été utilisé pour une autre source
		|			Alors on annote la source avec le sujet de l'autre source
		|		Sinon
		|			on annote le sujet pour cette source
		Sinon Si la source commence par un article indéterminé
			Alors c'est une source anonyme


CONTENU & LIBRAIRIES
------------------------------

	- LIBRAIRIES :

		Nous utilisons diverses librairies que nous allons détailler ici. Elles se trouvent dans le dossier CitationExtractor_lib.
		Ces librairies contiennent aussi des ressources utiles à la bonne utilisation du logiciel.

		* Stanford : 
			JAR : stanford-corenlp-3.6.0.jar, stanford-french-corenlp-2016-01-14-models.jar, hfst-ol.jar, slf4j-api.jar, slf4j-simple.jar
			RESSOURCES : lemmatizer-ressources/ (contenant plusieurs listes utiles Stanford/lemmatizer-ressources/dictionaries/fr) et ressources/
		* commons-collections4-4.1 :
			JAR : commons-cli-1.3.1.jar et commons-collections4-4.1.jar
		* GROBID :
			JAR : grobid-core-0.4.1-SNAPSHOT.jar
		* JSON :
			JAR : java-json.jar
		* MaltParser : 
			JAR : maltparser-1.9.0.jar
			RESSOURCES : MaltParser/model/ contient un modèle fremalt-1.7.mco de la langue française pour l'analyse syntaxique
		* wapiti_java : 
			JAR : wapiti_java/Wapiti-master/src/swig/wapiti-1.5.0.jar
			RESSOURCES : wapiti_java/Wapiti-master/build avec la librairie compilée pour linux libwapiti.so et celles pour windows libwapiti.dll et libwapiti_swig.dll.
	
	- DOSSIERS PRESENTS :

		Dans cette partie je vais énoncer quel dossier correspond à quelle étape de l'application. Surtout utile pour se retrouver dans la phase d'apprentissage pour pouvoir suivre
		la transformation des fichiers au fil de l'avancée.

		LINUX : 

		root = data_dir

		LABELED = root/labeled

			=> On trouve dans LABELED les dossiers suivants  :

				. input_labeled : comme son nom l'indique, on rentre tous les fichiers annotés dans celui-ci.
				. input_labeled_converted : ce dossier contiendra les fichiers .ann transformés pour n'avoir que les SOURCE-PRIM et SOURCE-SEC.
				. input_labeled_mp : ce dossier contiendra les fichiers .conll transformés après l'analyse syntaxique de MaltParser.

			OUTPUT_LABELED = LABELED/output_labeled

			=> On trouve dans le dossier OUTPUT_LABELED les dossiers suivants :

				. train_prim, train_sec, dev_prim, dev_sec, test_prim, test_sec : Fichier (B)IO transformés par rapport aux sources et offsets.

			RESULT_LABELED = LABELED/result_labeled

			=> On trouve dans le dossier RESULT_LABELED les dossiers suivants : 

				. bioWapiti_prim : les fichiers de sorties de wapiti .wapiti pour les sources primaires.
				. bioWapiti_sec : les fichiers de sorties de wapiti .wapiti pour les sources secondaires.
				. brat_autoWapiti : les fichiers de transformation des fichiers .wapiti en format BRAT .ann ET JSON .json_temp
				. brat_ref : les fichiers de références de sources pour les comparer avec les résultats .ann.
				. brat_ref_ajout_annotator : les fichiers transformés en .json et .ann avec l'ajout des annotations de coréférences.
				. merged_files : tous les fichiers de sortie (B)IO assemblés pour pouvoir faire l'entrainement et le test pour wapiti.
				. models : les modèles (pour prim et sec) qui s'écrivent après l'entrainement et utilisés pour la phase de test.
				. result : les résultats de l'évaluation de BratEval
				. /pattern_prim.txt : le template de pattern pour l'apprentissage des sources primaires.
				. /pattern_sec.txt : le template de pattern pour l'apprentissage des sources secondaires.

		UNLABELED = root/unlabeled

			=> On trouve les mêmes dossiers que LABELED

		FINAL = root/final

			=> On trouve dans FINAL les dossiers suivants : 

				. input : les fichiers que je veux convertir.
				. models : IMPORTANT contient les meilleurs modèles obtenus ! NE PAS LES EFFACER.
				. output : les fichiers de sortie (le fichier de sortie est configuré pour être dans root/final/output/ alors que l'input est n'importe quel fichier).

		

RESULTATS A RETROUVER
------------------------------

AFP SEULEMENT

510 files

test (39): 

afp.com-20150201T122209Z-TX-PAR-TVD26
afp.com-20151206T112542Z-TX-PAR-GEY94
afp.com-20151213T145323Z-TX-PAR-GUL86
afp.com-20151228T164632Z-TX-PAR-HSV30
afp.com-20151228T210444Z-TX-PAR-HTE47
afp.com-20160101T051525Z-TX-PAR-HXH85
afp.com-20160102T151732Z-TX-PAR-HYK74
afp.com-20160102T235505Z-TX-PAR-HYV91
afp.com-20160103T153701Z-TX-PAR-HZK62                                     BIO AFP FALSE : 
afp.com-20160107T095336Z-TX-PAR-IGM12									  Summary
afp.com-20160301T124801Z-TX-PAR-MNH54											         TP	FP	FN	Precision Recall	F1
afp.com-20160301T193359Z-TX-PAR-MOF25									  SOURCE-PRIM	157	16	43	0,9075	0,7850	0,8418
afp.com-20160302T181029Z-TX-PAR-MQK36									  SOURCE-SEC	16	4	10	0,8000	0,6154	0,6957
afp.com-20160303T152730Z-TX-PAR-MSK37                                     Overall	    173	20	53	0,8964	0,7655	0,8258
afp.com-20160305T081657Z-TX-PAR-MVW94
afp.com-20160305T145220Z-TX-PAR-MWI65
afp.com-20160307T221243Z-TX-PAR-NAO79
afp.com-20160308T081459Z-TX-PAR-NBB10									  IO AFP FALSE :	
afp.com-20160308T083306Z-TX-PAR-NBB66									  Summary								
afp.com-20160404T074249Z-TX-PAR-PBH62                                                    TP	FP	FN	Precision	Recall	F1
afp.com-20160408T143218Z-TX-PAR-PKP36                                     SOURCE-PRIM	158	22	43	0,8778	0,7861	0,8294
afp.com-20160408T203030Z-TX-PAR-PLK63                                     SOURCE-SEC	 18	4	8	0,8182	0,6923	0,7500	
afp.com-20160409T151212Z-TX-PAR-PMH31			                       	  Overall	    176	26	51	0,8713	0,7753	0,8205																		
afp.com-20160405T191011Z-TX-PAR-PEW28                                       									
afp.com-20160302T201335Z-TX-PAR-MQS38	                                                                            
afp.com-20160405T192315Z-TX-PAR-PEW80
afp.com-20160406T142824Z-TX-PAR-PGI09
afp.com-20160407T005304Z-TX-PAR-PHI82
afp.com-20160407T185241Z-TX-PAR-PJC09
afp.com-20160408T124247Z-TX-PAR-PKI77
afp.com-20160303T114511Z-TX-PAR-MRW54														
afp.com-20160308T083630Z-TX-PAR-NBB75
afp.com-20160308T104556Z-TX-PAR-NBH76
afp.com-20160309T081343Z-TX-PAR-NDH33
afp.com-20160309T125102Z-TX-PAR-NDU84
afp.com-20160310T010019Z-TX-PAR-NFB49
afp.com-20160310T111810Z-TX-PAR-NFS24
afp.com-20160401T101654Z-TX-PAR-OWS80
afp.com-20160402T061303Z-TX-PAR-OYI44


AFP + WEB

698 files 

test (55): 

20160106_02f4e570b3472de17bd65b7e11d39d1f.tag
20160202_058366bc34749ff88c69e2aa9cb98426.tag
afp.com-20151206T112542Z-TX-PAR-GEY94.tag
afp.com-20160303T114511Z-TX-PAR-MRW54.tag
afp.com-20160310T111810Z-TX-PAR-NFS24.tag
20160106_f356ae9c5a46a918d80a4c22768bbab0.tag
20160202_80be3bcbdeb1d92b729317390fbdbefe.tag         					  IO AFP + WEB FALSE :
afp.com-20151213T145323Z-TX-PAR-GUL86.tag								  Summary
afp.com-20160303T152730Z-TX-PAR-MSK37.tag								                 TP	FP	FN	Precision	Recall	F1
afp.com-20160401T101654Z-TX-PAR-OWS80.tag								  SOURCE-PRIM	203	26	74	0,8865	0,7329	0,8024
20160108_5d371d359dc75ecc539151798ac6e214.tag							  SOURCE-SEC	 11	7	22	0,6111	0,3333	0,4314
20160202_92892bda024e1d3d373bbf8b85a0f5a0.tag							  Overall	    214	33	96	0,8664	0,6903	0,7684
afp.com-20151228T164632Z-TX-PAR-HSV30.tag
afp.com-20160305T081657Z-TX-PAR-MVW94.tag
afp.com-20160402T061303Z-TX-PAR-OYI44.tag
20160112_4d7fd6746c2d713a6e3f610d0ee76ba2.tag
20160202_d2c59e8f1d37268664a405f98227228e.tag
afp.com-20151228T210444Z-TX-PAR-HTE47.tag
afp.com-20160305T145220Z-TX-PAR-MWI65.tag
afp.com-20160404T074249Z-TX-PAR-PBH62.tag
20160112_529c39d4d93b067f17e420e4905447d9.tag
20160203_844e8bdd633c7a84b7f048cbd1f892fc.tag
afp.com-20160101T051525Z-TX-PAR-HXH85.tag
afp.com-20160308T081459Z-TX-PAR-NBB10.tag
afp.com-20160405T191011Z-TX-PAR-PEW28.tag                                 BIO AFP + WEB FALSE :
20160115_318c2de3ba442701fc427bc59526329f.tag                             Summary
20160203_a9555cf46661505709846ecd5997b652.tag										   TP	FP	FN	Precision	Recall	F1
afp.com-20160102T151732Z-TX-PAR-HYK74.tag								  SOURCE-PRIM  209	21	69	0,9087	0,7518	0,8228
afp.com-20160308T083306Z-TX-PAR-NBB66.tag								  SOURCE-SEC	7	0	26	1,0000	0,2121	0,3500
afp.com-20160405T192315Z-TX-PAR-PEW80.tag								  Overall	   216	21	95	0,9114	0,6945	0,7883
20160115_dc89ecc80d3b722d7675714517d346d8.tag
20160203_d7f472ee3c703f652c476088f17c705e.tag
afp.com-20160103T153701Z-TX-PAR-HZK62.tag
afp.com-20160308T083630Z-TX-PAR-NBB75.tag
afp.com-20160406T142824Z-TX-PAR-PGI09.tag
20160127_3577bf63e3c4efebfc135a2176ea7f3b.tag
20160204_893f2f857f52f8126c369b3c73aa51c0.tag
afp.com-20160107T095336Z-TX-PAR-IGM12.tag
afp.com-20160308T104556Z-TX-PAR-NBH76.tag
afp.com-20160407T005304Z-TX-PAR-PHI82.tag
20160127_9e8bacbcb17e10afa275ed0676fca292.tag
20160204_eba5c36feccb3ca4d8d8a2eb3801bf86.tag
afp.com-20160301T193359Z-TX-PAR-MOF25.tag
afp.com-20160309T081343Z-TX-PAR-NDH33.tag
afp.com-20160407T185241Z-TX-PAR-PJC09.tag
20160127_b0cb9bc083ec9283d04931d2f248eaf7.tag
20160327_a166436d01a33e64ab157b37bd442874.tag
afp.com-20160302T181029Z-TX-PAR-MQK36.tag
afp.com-20160309T125102Z-TX-PAR-NDU84.tag
afp.com-20160408T124247Z-TX-PAR-PKI77.tag
20160128_3e69b65d217949814f9a9be66f637bec.tag
20160327_dad1e8e0bc0705190f9d5216e2c1cf16.tag
afp.com-20160302T201335Z-TX-PAR-MQS38.tag
afp.com-20160310T010019Z-TX-PAR-NFB49.tag
afp.com-20160409T151212Z-TX-PAR-PMH31.tag


FUTUR
------------------------------

	- EVALUATION CORREFERENCES : 

		Nous n'avons pas eu le temps d'évaluer les coréférences obtenues.
		Dans SourceExtractor/SourcesExtractorLinux/dir/Outils/normalizeBratRef, il y a un script perl normalizeBratReferenceRelations.pl qui permet de corriger l'annotation manuelle
		des coréférences sur Brat.

	- DBPEDIA et ENTITY LINKING :

		Nous avons essayé de mettre DBPEDIA pour faire de l'entity linking mais les résultats n'étaient pas excellents, ou du moins cela ne menait à rien d'interressant lors de notre
		implémentation.
		Dans le dossier ANNEXES/DBPEDIA se trouvent tous les fichiers utiles pour réintégrer Dbpedia dans le code (SaxParser). Il y a une ancienne version du code
		qui l'intègre à peu près réutilisable.

	- PATTERN POUR WAPITI :

		Les pattern utilisés sont peut-être à modifier pour avoir de meilleurs résultats (ou de moins bons...). Cela prend du temps !

	- SOURCES SECONDAIRE : 

		Il faudrait essayer d'enlever toutes les phrases qui ne contiennent pas de sources primaire pour l'entrainement des sources secondaire.

	- MEDIAS :

		Trouver une solution efficace pour charger la liste des médias pour ne pas perdre 2 min à chaque début de script.

	- APPLICATION SUR DOSSIER :

		Il faudrait permettre de lancer l'application sur des dossiers contenant des fichiers .txt. Attention beaucoup de code à modifier, ce n'est pas "simple".
		Le code prend en compte cette future modification car les fonctions qui lisent un fichier regardent d'abord si c'est isFile() ou isDirectory().

	- CODE :

		Evidemment on pourrait simplifier le code et le rendre orienté objet (sortie BRAT, JSON, ...). 
		Améliorer et réduction de l'algorithme pour les coréférences. Cela marche pas mal mais il y a surement des réductions à faire.

	- WINDOWS ET LINUX :

		Réussir à faire une seule application.

		J'ai été bloqué car je me suis retrouver avec deux .jar qui contenaient les mêmes packages (fr.limsi.wapiti) mais pas les mêmes classes / méthodes à l'intérieur de chacun.

		J'ai essayé d'utiliser un ClassLoader mais je me retrouvais toujours avec les méthodes du .jar déjà intégrées.
		J'ai essayé d'utiliser deux ClassLoader mais j'ai une classe qui extends une classe se trouvant dans un .jar et je n'ai pas trouvé / eu le temps de 
		chercher comment faire pour étendre une classe issue d'un ClassLoader.
		J'ai essayé jarjar qui permet de modifier les noms de packages à l'intérieur d'un .jar compilé (il modifie les package dans les .class) mais on se retrouvait
		avec un UnsatisfiedLinkedError d'une fonction native.




Pour tous problèmes ou questions, je reste entièrement joignable par mail : gabriel.bellard@gmail.com